import os
import logging

from lab import tools



def filter(exppath, configs=[]):
    prop_file = os.path.join(exppath.rstrip('/') + '-eval',  'properties')
    if (not os.path.exists(prop_file)):
        logging.critical('No properties file found at "%s"' % prop_file)
    props = tools.Properties(prop_file)

    exps = {}
    for run in props:
        entry = props[run]

        run_id = entry['id']
        if not run_id:
            # Abort if an id cannot be read.
            logging.critical('id is not set in %s.' % prop_file)

        nick = entry['algorithm']
        domain = entry['domain']
        instance = run_id[2]

        if (not exps.has_key(domain)):
            exps[domain] = {}

        if (not exps[domain].has_key(instance)):
            exps[domain][instance] = {}

        if (exps[domain][instance].has_key(nick)):
            logging.critical("algorithm {nick} has already been checked for {domain} {instance}".format(**locals()))

        exps[domain][instance][nick] = {}
        exps[domain][instance][nick]['run'] = run

        relevant_run = nick in configs or not configs
        relevant_run = relevant_run and "translator_time_done" in entry
        exps[domain][instance][nick]['relevant'] = relevant_run
        if (relevant_run):
            exps[domain][instance][nick]['abstained'] = entry['abstained'] if "abstained" in entry else (entry["number_leaf_factors"] < 2 if "number_leaf_factors" in entry else False)
    for domain in exps:
        for inst in exps[domain]:
            all_abstained = True
            for nick in exps[domain][inst]:
                if (exps[domain][inst][nick]['relevant'] and not exps[domain][inst][nick]['abstained']):
                    all_abstained = False
                    break
            if (all_abstained or domain == 'movie'):
                for nick in exps[domain][inst]:
                    del props[exps[domain][inst][nick]['run']]
    props.write()
