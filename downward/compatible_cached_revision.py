# -*- coding: utf-8 -*-
#
# Downward Lab uses the Lab package to conduct experiments with the
# Fast Downward planning system.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import glob
import hashlib
import logging
import os.path
import shutil
import subprocess

from lab import tools

from cached_revision import *


_HG_ID_CACHE = {}



class CompatibleCachedRevision(CachedRevision):
    """This class represents Fast Downward checkouts.

    It provides methods for caching and compiling given revisions.
    """
    def __init__(self, repo, local_rev, build_options):
        """
        * *repo*: Path to Fast Downward repository.
        * *local_rev*: Fast Downward revision.
        * *build_options*: List of build.py options.
        """
        # only default initialization for now
        CachedRevision.__init__(self, repo=repo, local_rev=local_rev, build_options=build_options)
        
    def cache(self, revision_cache):
        self._path = os.path.join(revision_cache, self._hashed_name)
        if os.path.exists(self.path):
            logging.info('Revision is already cached: "%s"' % self.path)
            if not os.path.exists(self._get_sentinel_file()):
                logging.critical(
                    'The build for the cached revision at {} is corrupted '
                    'or was made with an older Lab version. Please delete '
                    'it and try again.'.format(self.path))
        else:
            tools.makedirs(self.path)
            excludes = ['-X{}'.format(d) for d in ['benchmarks', 'experiments', 'misc']]
            retcode = tools.run_command(
                ['hg', 'archive', '-r', self.global_rev] + excludes + [self.path],
                cwd=self.repo)
            if retcode != 0:
                shutil.rmtree(self.path)
                logging.critical('Failed to make checkout.')
            self._compile()
            self._cleanup()

    def get_planner_resource_name(self):
        return 'fast_downward_' + self._hashed_name

    def _compile(self):
        if not os.path.exists(os.path.join(self.path, "src", 'build_all')):
            logging.critical('build_all not found.')
        retcode = tools.run_command(
            ['./build_all'] + self.build_options, cwd=os.path.join(self.path, "src/"))
        if retcode == 0:
            tools.write_file(self._get_sentinel_file(), '')
        else:
            logging.critical('Build failed in {}'.format(self.path))

    def _cleanup(self):
        # Remove unneeded files.
        tools.remove_path(self.get_cached_path("src/build_all"))
        tools.remove_path(self.get_cached_path("src/cleanup"))
        tools.remove_path(self.get_cached_path("src/validate"))
        # Remove unneeded folder.
        tools.remove_path(self.get_cached_path("src/VAL/"))
        # Remove .obj files.
        tools.remove_path(self.get_cached_path("src/preprocess/.obj/"))
        tools.remove_path(self.get_cached_path("src/search/.obj/"))

        # Strip binaries.
        if ("--debug" in self.build_options):
            binaries = [os.path.join(self.path, "src", "preprocess", "preprocess-debug"),
                        os.path.join(self.path, "src", "search", "downward-debug")]
        else:
            binaries = [os.path.join(self.path, "src", "preprocess", "preprocess"),
                        os.path.join(self.path, "src", "search", "downward-release")]
        subprocess.call(['strip'] + binaries)
