#!/usr/bin/env python
# encoding: utf-8

from lab.reports import Attribute

COMMON = ['search_returncode', 'error', 'search_time', 'evaluations', 'expansions', 'coverage', 'total_time', 'generated']

OPT = ['expansions_until_last_jump', 'evaluations_until_last_jump', 'memory', 'plan_length', 'cost', 'reopened']
SAT = ['memory', 'plan_length', 'cost']
UNSAT = [Attribute('proved_unsolvable', absolute=True, min_wins=False), 'raw_memory']

STATE_SPACE_SIZE = ['stored_state_variable_values', 'decoupled_state_space_size_1', 'decoupled_state_space_size_2', 'decoupled_state_space_size_3', 'decoupled_state_space_size_4']

POR = [Attribute('por_number_pruned_transitions', min_wins=False), Attribute('por_percentage_pruned_transitions', min_wins=False)]

SYMMETRY = ['number_generators']
SYMBOLIC = []

DECOUPLED_SEARCH = [Attribute('abstained', absolute=True), 'max_reachable_factor_size', 'avg_reachable_factor_size', 'min_reachable_factor_size', Attribute('number_leaf_factors', absolute=False, min_wins=False), 'min_factor_size', 'avg_factor_size', 'max_factor_size', 'factoring_time', 'max_number_dups']

LP_FACTORINGS = ["min_flexibility", "max_flexibility", "avg_flexibility", "sum_flexibility", "min_mobility", "max_mobility", "avg_mobility", "sum_mobility", "num_lp_variables", "num_lp_constraints", "lp_solving_time", "lp_construction_time"]

SELF_CONFIGURATION = ['final_factoring_name']

SUBDOMINIZATION = [Attribute('proved_unsolvable', absolute=True, min_wins=False), "translator_returncode", "translator_task_size", "translator_auxiliary_atoms", "translator_facts", "translator_final_queue_length", "translator_mutex_groups", "translator_operators", "translator_operators_removed", "translator_peak_memory", "translator_relevant_atoms", "translator_time_completing_instantiation", "translator_time_computing_fact_groups", "translator_time_computing_model", "translator_time_done", "translator_time_instantiating", "translator_time_parsing", "translator_total_queue_pushes", "translator_uncovered_facts", "translator_variables", 'complete_time_to_fail', 'complete_time', 'translator_actions_instantiated', 'search_wall_clock_time', 'translator_actions_instantiated_on_success', "incremental_grounding_total_time_success", "real_translator_time_done_incremental", "real_search_time_incremental", "real_total_time_incremental", "real_translator_relevant_atoms", "real_translator_auxiliary_atoms", "real_translator_final_queue_length", "real_translator_total_queue_pushes", "real_translator_uncovered_facts", "real_translator_effect_conditions_simplified", "real_translator_implied_preconditions_added", "real_translator_operators_removed", "real_translator_axioms_removed", "real_translator_propositions_removed", "real_translator_actions_instantiated", "real_complete_time_incremental"]
